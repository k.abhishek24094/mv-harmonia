FROM alpine:latest
RUN apk update
RUN apk add tzdata && cp /usr/share/zoneinfo/Asia/Calcutta /etc/localtime && echo "Asia/Calcutta" > /etc/timezone
RUN apk add openjdk8 && apk add curl
WORKDIR /usr/share/
ADD https://mv-deployment-packages.s3.ap-south-1.amazonaws.com/elk/bajaj_tomcat8.tar .
RUN tar -xvf bajaj_tomcat8.tar
ADD https://s3.ap-south-1.amazonaws.com/staging.whizdm.txns/OB-harmonia-14.12.21-SNAPSHOT-2021-12-14T06:51:19Z.war  ./tomcat8/webapps/
RUN mv ./tomcat8/webapps/OB-harmonia-14.12.21-SNAPSHOT-2021-12-14T06:51:19Z.war ./tomcat8/webapps/ROOT.war
EXPOSE 8080
#Healthcheck
HEALTHCHECK --interval=60s --timeout=5s CMD curl -f http://127.0.0.1:8080/health || exit 1
#CMD ["ls","-la"]
CMD ["sh","./tomcat8/bin/catalina.sh","run"]
